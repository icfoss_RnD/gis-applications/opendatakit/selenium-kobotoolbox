package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.io.FileReader;
import au.com.bytecode.opencsv.CSVReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RegisterUser {

	static Map<String, String> XPATH_MAP = new HashMap<String, String>();

	static String REGUSER_URL = "https://koboform.kobo.keralarescue.in/accounts/register";

	private static final String LOGIN_FILE_PATH = "/home/user/apps/life/kobo/xls_upload/localbodies-KR-test.csv";
	
	static String REG_COMMON_EMAIL = "floodkerala2018@gmail.com";
	
	public RegisterUser(){
		//Add user page parameters
		XPATH_MAP.put("USERNAME_FIELD","//*[@id=\"id_username\"]");
		XPATH_MAP.put("EMAIL_FIELD","//*[@id=\"id_email\"]");
		XPATH_MAP.put("PASSWORD_FIELD", "//*[@id=\"id_password1\"]");
		XPATH_MAP.put("CNF_PASSWORD_FIELD", "//*[@id=\"id_password2\"]");
		XPATH_MAP.put("REG_BTN", "/html/body/div/form[2]/div[2]/input[2]");
	}
	
	public void readUsers(WebDriver driver) throws Exception{
		driver.get(REGUSER_URL);
		driver.manage().window().maximize();

		CSVReader reader = new CSVReader(new FileReader(LOGIN_FILE_PATH));
		String [] nextLine;
		nextLine = reader.readNext();
		while ((nextLine = reader.readNext()) != null) {
			try{
			String username = nextLine[0];
			//for registration, username should be lowercase
			username = username.toLowerCase();
			String name = nextLine[1];
			String password = nextLine[2];
			RegisterUser.regUser(driver,username,password);
			TimeUnit.SECONDS.sleep(10);	//10 second delay
//			driver.navigate().refresh(); //refresh page
			WebElement reg_message_field=driver.findElement(By.xpath("/html/body/div/form/p[1]"));
			String message = reg_message_field.getText(); //here it will match that if that button is present or not. If present it will continue execution from the next line or if it is not present the execution stops there and your test case fails.
			if(! message.contains("Thanks for creating a KoBoToolbox account")){
				System.out.print("SOME ERROR IN REGISTRATION!");
				break;
			}
			System.out.println(username+","+name+","+password);
			} catch(Exception e){
				System.out.println(e.getMessage());
				TimeUnit.SECONDS.sleep(5);	//5 second delay
				driver.navigate().refresh(); //refresh page
			}
		}
		reader.close();
	}
	
	
	public static void regUser(WebDriver driver, String username, String password){
		driver.get(REGUSER_URL);
		WebElement username_field = driver.findElement(By.xpath(XPATH_MAP.get("USERNAME_FIELD")));
		username_field.clear();
		username_field.sendKeys(username);
		WebElement email_field = driver.findElement(By.xpath(XPATH_MAP.get("EMAIL_FIELD")));
		email_field.clear();
		email_field.sendKeys(REG_COMMON_EMAIL);
		WebElement password_field = driver.findElement(By.xpath(XPATH_MAP.get("PASSWORD_FIELD")));
		password_field.clear();
		password_field.sendKeys(password);
		WebElement cnfpassword_field = driver.findElement(By.xpath(XPATH_MAP.get("CNF_PASSWORD_FIELD")));
		cnfpassword_field.clear();
		cnfpassword_field.sendKeys(password);
		driver.findElement(By.xpath(XPATH_MAP.get("REG_BTN"))).click();
//		System.out.println("created account!! "+username);
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// geckodriver for firefox
//		System.setProperty("webdriver.gecko.driver", "/home/user/bin/geckodriver");
		//chromedriver
		System.setProperty("webdriver.chrome.driver", "/home/user/bin/chromedriver");
		//driver firefox
//		WebDriver driver=new FirefoxDriver();
		ChromeOptions options = new ChromeOptions();
//		options.addArguments("--incognito");
		//driver chrome
		WebDriver driver=new ChromeDriver(options);
		//waiting implicitly for each page loading
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		RegisterUser obj = new RegisterUser();
		obj.readUsers(driver);
	}

}
