package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.io.FileReader;
import au.com.bytecode.opencsv.CSVReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class FormUpload {
	
	static Map<String, String> XPATH_MAP = new HashMap<String, String>();

	static String LOGIN_URL = "https://koboform.kobo.keralarescue.in/accounts/login/?next=/";

	private static final String LOGIN_FILE_PATH = "/home/user/apps/life/kobo/xls_upload/localbodies-KR-test.csv";
	private static final String XLSFORM_FILE_PATH = "/home/user/apps/life/kobo/xls_upload/";
	
	public FormUpload(){
		//First page parameters
		XPATH_MAP.put("USERNAME_FIELD","//*[@id=\"id_username\"]");
		XPATH_MAP.put("PASSWORD_FIELD", "//*[@id=\"id_password\"]");
		XPATH_MAP.put("LOGIN_BTN", "/html/body/div/form/input[2]");
		
		//form upload parameters
		XPATH_MAP.put("NEW_BTN", "/html/body/div/div/div[2]/div[1]/div[1]/div/button");
		XPATH_MAP.put("UPLOAD_XLS_BTN", "/html/body/div/div/div[2]/div[1]/div/div/div/form/div/button[3]");
		XPATH_MAP.put("UPLOAD_INPUT_FIELD", "/html/body/div/div/div[2]/div[1]/div/div/div/form/div/input");
		XPATH_MAP.put("PROJECT_NAME_INPUT_FIELD", "//*[@id=\"name\"]");
		XPATH_MAP.put("CREATE_PROJECT_BTN", "/html/body/div/div/div[2]/div[1]/div/div/div/form/div/footer/button");
		
		//form deploy parameter
		XPATH_MAP.put("DEPLOY_BTN",	"/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[2]/a");
		
		//logout parameters
		XPATH_MAP.put("PROFILE_DROPDOWN", "/html/body/div/div/div[2]/header/div[1]/div[2]/div/a");
		XPATH_MAP.put("LOGOUT_BTN", "/html/body/div/div/div[2]/header/div[1]/div[2]/div/div/ul/li[3]/a");
	}
	
	public void readUsers(WebDriver driver) throws Exception{
		CSVReader reader = new CSVReader(new FileReader(LOGIN_FILE_PATH));
		String [] nextLine;
		nextLine = reader.readNext();
		while ((nextLine = reader.readNext()) != null) {
//			try{
			String username = nextLine[0];
//			System.out.println("id!! "+id);
			String name = nextLine[1];
			String password = nextLine[2];
			FormUpload.doLogin(driver,username,password);
			TimeUnit.SECONDS.sleep(5);	//5 second delay
			driver.navigate().refresh(); //refresh page
			TimeUnit.SECONDS.sleep(5);	//5 second delay
			FormUpload.formUpload(driver,username);
			TimeUnit.SECONDS.sleep(5);	//5 second delay
			FormUpload.doLogout(driver);
			TimeUnit.SECONDS.sleep(5);	//5 second delay
			driver.navigate().refresh(); //refresh page
			TimeUnit.SECONDS.sleep(5);	//5 second delay
			System.out.println(username+","+name+","+password+",1");
//			} catch(Exception e){
//				System.out.println(e.getMessage());
//				TimeUnit.SECONDS.sleep(5);	//5 second delay
//				driver.navigate().refresh(); //refresh page
//			}
		}
		reader.close();
	}
	
	public static void formUpload(WebDriver driver, String username) throws Exception{
		driver.findElement(By.xpath(XPATH_MAP.get("NEW_BTN"))).click();
		driver.findElement(By.xpath(XPATH_MAP.get("UPLOAD_XLS_BTN"))).click();
		//Upload the xlsform
		WebElement uploadElement = driver.findElement(By.xpath(XPATH_MAP.get("UPLOAD_INPUT_FIELD")));
		String xlsform = XLSFORM_FILE_PATH + "2018 Flood Data Collection.xlsx";
		uploadElement.sendKeys(xlsform);
//		System.out.println("uploaded form!! ");
		TimeUnit.SECONDS.sleep(5);	//5 second delay
		// Clear exisint name and update project name prepending username
		String project_name = username + " - 2018 Flood Data Collection";
		driver.findElement(By.xpath(XPATH_MAP.get("PROJECT_NAME_INPUT_FIELD"))).clear();
		TimeUnit.SECONDS.sleep(1);	//1 second delay after deployment
		driver.findElement(By.xpath(XPATH_MAP.get("PROJECT_NAME_INPUT_FIELD"))).sendKeys(project_name);
		// create project
		driver.findElement(By.xpath(XPATH_MAP.get("CREATE_PROJECT_BTN"))).click();
		TimeUnit.SECONDS.sleep(5);	//5 second delay after deployment
		driver.navigate().refresh(); //refresh page
		TimeUnit.SECONDS.sleep(5);	//5 second delay after deployment
		// deploy form
		driver.findElement(By.xpath(XPATH_MAP.get("DEPLOY_BTN"))).click();
//		System.out.println("deployed form!! ");
		TimeUnit.SECONDS.sleep(5);	//5 second delay after deployment
	}
	
	public static void doLogin(WebDriver driver, String username, String password){
		driver.get(LOGIN_URL);
		driver.manage().window().maximize();
		driver.findElement(By.xpath(XPATH_MAP.get("USERNAME_FIELD"))).sendKeys(username);
		driver.findElement(By.xpath(XPATH_MAP.get("PASSWORD_FIELD"))).sendKeys(password);
		driver.findElement(By.xpath(XPATH_MAP.get("LOGIN_BTN"))).click();
//		System.out.println("loggedin!! "+username);
	}

	public static void doLogout(WebDriver driver){
		driver.findElement(By.xpath(XPATH_MAP.get("PROFILE_DROPDOWN"))).click();
		driver.findElement(By.xpath(XPATH_MAP.get("LOGOUT_BTN"))).click();
//		System.out.println("loggedout!!");
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// geckodriver for firefox
//		System.setProperty("webdriver.gecko.driver", "/home/user/bin/geckodriver");
		//chromedriver
		System.setProperty("webdriver.chrome.driver", "/home/user/bin/chromedriver");

		//driver firefox
//		WebDriver driver=new FirefoxDriver();

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		//driver chrome
		WebDriver driver=new ChromeDriver(options);
		//waiting implicitly for each page loading
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		FormUpload obj = new FormUpload();
		obj.readUsers(driver);
	}

}
