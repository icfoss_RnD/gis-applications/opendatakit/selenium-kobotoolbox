package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.io.FileReader;
import au.com.bytecode.opencsv.CSVReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CreateUser {

	static Map<String, String> XPATH_MAP = new HashMap<String, String>();

	static String LOGIN_URL = "https://kobo.keralarescue.in/";

	static String ADDUSER_URL = "https://kobocat.kobo.keralarescue.in/admin/auth/user/add/";

	private static final String LOGIN_FILE_PATH = "/home/user/apps/life/kobo/xls_upload/localbodies-KR-test.csv";
	
	public CreateUser(){
		//Add user page parameters
		XPATH_MAP.put("USERNAME_FIELD","//*[@id=\"id_username\"]");
		XPATH_MAP.put("PASSWORD_FIELD", "//*[@id=\"id_password1\"]");
		XPATH_MAP.put("CNF_PASSWORD_FIELD", "//*[@id=\"id_password2\"]");
		XPATH_MAP.put("ADD_OTHER_BTN", "//*[@id=\"user_form\"]/div/div/input[2]");
	}
	
	public void readUsers(WebDriver driver) throws Exception{
		driver.get(ADDUSER_URL);
		driver.manage().window().maximize();
		CSVReader reader = new CSVReader(new FileReader(LOGIN_FILE_PATH));
		String [] nextLine;
		nextLine = reader.readNext();
		while ((nextLine = reader.readNext()) != null) {
			try{
			String username = nextLine[0];
			String name = nextLine[1];
			String password = nextLine[2];
			CreateUser.addUser(driver,username,password);
			TimeUnit.SECONDS.sleep(1);	//1 second delay
//			driver.navigate().refresh(); //refresh page
			System.out.println(username+","+name+","+password);
			} catch(Exception e){
				System.out.println(e.getMessage());
				TimeUnit.SECONDS.sleep(1);	//5 second delay
				driver.navigate().refresh(); //refresh page
			}
		}
		reader.close();
	}
	
	
	public static void addUser(WebDriver driver, String username, String password){
		driver.findElement(By.xpath(XPATH_MAP.get("USERNAME_FIELD"))).sendKeys(username);
		driver.findElement(By.xpath(XPATH_MAP.get("PASSWORD_FIELD"))).sendKeys(password);
		driver.findElement(By.xpath(XPATH_MAP.get("CNF_PASSWORD_FIELD"))).sendKeys(password);
		driver.findElement(By.xpath(XPATH_MAP.get("ADD_OTHER_BTN"))).click();
//		System.out.println("created account!! "+username);
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// geckodriver for firefox
//		System.setProperty("webdriver.gecko.driver", "/home/user/bin/geckodriver");
		//chromedriver
		System.setProperty("webdriver.chrome.driver", "/home/user/bin/chromedriver");
		//driver firefox
//		WebDriver driver=new FirefoxDriver();
		ChromeOptions options = new ChromeOptions();
//		options.addArguments("--incognito");
		//driver chrome
		WebDriver driver=new ChromeDriver(options);
		//waiting implicitly for each page loading
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get(LOGIN_URL);
		driver.findElement(By.xpath("//*[@id=\"id_username\"]")).sendKeys("kobo");
		driver.findElement(By.xpath("//*[@id=\"id_password\"]")).sendKeys("!tisK0b0");
		driver.findElement(By.xpath("/html/body/div/form/input[2]")).click();

		CreateUser obj = new CreateUser();
		obj.readUsers(driver);
	}

}
